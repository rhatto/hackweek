# About the project

 * Contact: <YOUR NAME HERE>
 * Chat: #tor-<FILL IN> on `irc.oftc.net`
 * Video room: https://tor.meet.coop/<FILL IN>

# Participants

- <YOUR NAME HERE>
- etc

# Summary

A description of the work that you want to do this week.

## Project A

## Project B

## etc

# Skills

What are the skills needed for the project? For example, somebody that
knows onion services, a designer, a translator, etc.

# Links

/label ~Documentation ~Proposal
