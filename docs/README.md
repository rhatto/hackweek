# Tor's Hackweek

## About

This repository is being used to document and coordinate hackweeks at Tor.

## Previous Hackweeks

Old Hackweek documentation is archived in [past](past/README.md) folder. Examples:

* [March/April 2021](past/2021/README.md)
* [June 2022](past/2022/README.md)
* [November 2023](past/2023/README.md)
