# Tor-Split-Tunneling for Bitmask

## General Info

* MEETING PLACE: We will be around on irc #leap on libera.chat, you can ping cyberta1[m] there to join or if you have any questions
* cyberta coordinates this hackweek project
* the project starts right after the presentation on monday 16 utc
* meeting room: https://vc.autistici.org/bitmask-tor-hackweek-22 (first meeting 17:10 UTC, today)
* irc #leap on libera irc://irc.libera.chat/leap

## Team and Who is Diong WhatG

* Cyberta: integrate outline-go-tunsocks library. start / stop tor.
* kwadronaut: UX lol and ..
* Lindsey: Go development https://github.com/Jigsaw-Code/outline-go-tun2socks
* Ankit: Will start with UX once its defined
* Mcnair: UX

## Proposal

Bitmask is VPN client based on OpenVPN and Pluggable Transports to route traffic. It currently integrates Tor and Snowflake to circumvent blocking attempts of the bootstrapping phase, when a VPN tunnel cannot be established yet.

We want to extend the existing tor functionality in Bitmask by allowing users to choose to either route traffic of a specific app over tor or over openvpn.

This brings several adavantages:

* users can choose between a high degree of anonymity or a faster connection
* users can always use tor and Snowflake as a backup, if circumventing censorship with openvpn + obfs4 fails
* users that use tor to protect their traffic against ISP's, Big Data companies or are connected to public wifis have a backup solution for apps using udp traffic like audio- and video-conferencing apps

We foresee different tasks and and related skill-sets:

* wireframing for the UI on Bitmask: we need a way to let the user choose which app should be routed over which transport (UX expertise)
* the implementation of that UI in Android (Java, Android SDK)
* the integration of go socks2tun or a similar lib to forward packets from an file descriptor (the tun interface) to the socks port of android (golang, gomobile, Java)
* the integration of our existing packet parsing Java code used in onionmasq which checks the origin of a packet and maps it to an application ID (Java, Android SDK)
* extending the packet parsing Java code to either forward packets to the socks interface or to the file descriptor handled by openvpn
* in parallel: a rewrite for 4. and 5. in golang, which we can use in the future. This task requires some more work but shouldn't stop us from creating a prototype within a week (golang, preferred some network-level programming skills)

Let's do it!

## UX considerations:

- Bitmask currently supports app-exclusion - selected apps are not routed over the VPN
- a new list for openvpn/tor routing shouldn't show excluded apps to choose
- good / expected defaults: this feature is ment for advanced users, the app shouldn't require any configuration to start with, defaulting to openvpn, settings entry somewhere in advanced settings
- tor and openvpn are mutually exclusive, we don't want support vpn over tor or vice versa
- shall we default to tor+snowflake?
- Or do we want to reuse the snowflake settings for tor network traffic routing?
- if tor routing is enabled for at least 1 app, the tor state should be shown in the app
- tor routing is only available as long as the VPN is running (there's no technical requirement for it though)
- if openvpn+obfs4 is blocked, the user could be asked if all traffic should be routed over tor+snowflake

tun2socks
https://github.com/Jigsaw-Code/outline-go-tun2socks
https://github.com/FlowerWrong/tun2socks/tree/master/tun2socks
https://github.com/eycorsican/leaf (although written in rust)
https://github.com/guardianproject/Orbot-IPtProxy/blob/master/IPtProxy.go/IPtProxy.go#L358 ff.

useful links to related repositories
https://gitlab.torproject.org/tpo/core/onionmasq/-/tree/main/android/ArtiToyVPN/app/src/main/java/org/torproject/artitoyvpn/capture

https://0xacab.org/leap/android_libs

https://0xacab.org/leap/bitmask_android/

https://github.com/Jigsaw-Code/outline-go-tun2socks/blob/master/tunnel/tun.go

Mcnair Questions and thoughts

Questions
* when vpn doesnt work, can the app tell that its because of censorship versus the useres network being down or the service provider gateway being down? (-> difficult to distinguish, some cases like missing internet connection is easy though, we assume that if connecting fails of different servers with the same transport that the user is in a censored network)
* related to above: is the user promoted to try circumvention tech when they cant connect, or we just assume that they know. -> on Android, the user is asked to use bridges if plain openvpn gateways (plural!) fail (for whatever reason)
* if the app is unable to connect we could have the splash page that shows up directing people to try circumvention. -> we currently have already an alert, see above, that could be done nicer UI wise, but the general flow already exists
* can the list of excluded app just say: "These apps will be excluded across alls Transport and circumvention options that you select" yes
* why is this proposal focused on specific apps instead of all over openVPN or all over Tor? -> because tor has performance hits, doesn't support udp (jitsi etc.), might be an option for users that want to use tor for it's anonymity features with specific apps
* Is the ability to route using Tor a new anonimity setting or is it another circumvention tech option? -> both, since we can also run tor over snowflake for anti-censorship (tor is quite often blocked these days in regions with heavy censorship without Pluggable Transports like snowflake), it is probably more often an anonymity feature, and can be used as a censorship circumvention fallback

User Flow
1) Noncensored environment - OpenVPN with UDP for fastest. TCP for loseless
2) Censored environements options

    a) OpenVPN (UDP or TCP) + Snowflake option (for API communication) and Obsf4 option (for network traffic routing)

    b) Tor + Snowflake for network traffic routing
